# JEMM386

Jemm386 is an Expanded Memory Manager for DOS, based on the source of FreeDOS EMM386. It has several advantages compared to FreeDOS and/or MS-DOS EMM386. * needs only 192 bytes DOS memory * needs very little extended memory * is faster * works with MS-DOS, FreeDOS and EDR-DOS * will use features implemented on later CPUs to further increase speed * unlike FreeDOS Emm386 Jemm386 has full VDS support * can be loaded from the command line


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## JEMM.LSM

<table>
<tr><td>title</td><td>JEMM386</td></tr>
<tr><td>version</td><td>5.79d</td></tr>
<tr><td>entered&nbsp;date</td><td>2020-05-18</td></tr>
<tr><td>description</td><td>Jemm386 is an Expanded Memory Manager for DOS</td></tr>
<tr><td>summary</td><td>Jemm386 is an Expanded Memory Manager for DOS, based on the source of FreeDOS EMM386. It has several advantages compared to FreeDOS and/or MS-DOS EMM386. * needs only 192 bytes DOS memory * needs very little extended memory * is faster * works with MS-DOS, FreeDOS and EDR-DOS * will use features implemented on later CPUs to further increase speed * unlike FreeDOS Emm386 Jemm386 has full VDS support * can be loaded from the command line</td></tr>
<tr><td>keywords</td><td>HIMEM + EMM386, STABLE, COMPATIBILITY, memory manager, jemmex, himemx</td></tr>
<tr><td>author</td><td>Japheth, Tom Ehlert, Michael Devore (FreeDosStuff -at- devoresoftware.com)</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/Baron-von-Riedesel/Jemm</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Artistic License</td></tr>
</table>
